﻿using Caliburn.Micro;
using MyWpf.AppCommon;
using MyWpf.Components.Instructions;
using MyWpf.Components.MarketPrices;

namespace MyWpf
{
    public class MainViewModel : BaseManagerViewModel
    {
        public MainViewModel(IWindowManager windowManager) : base(windowManager) { }

        public void OnOpenInstruction()
        {
            ShowWindow<InstructionManagerViewModel>();
        }

        public void OnOpenMarketPrice()
        {
            ShowWindow<MarketPriceViewModel>();
        }
    }
}
