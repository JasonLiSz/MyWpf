﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using Caliburn.Micro;
using MyWpf.AppCommon;
using MyWpf.Common.MessageBroker;
using MyWpf.Components.Instructions.Context;
using MyWpf.Entities;
using MyWpf.Providers;
using MyWpf.SysCommon;

namespace MyWpf.Components.Instructions
{
    public class InstructionManagerViewModel : BaseManagerViewModel
    {
        public string SearchText
        {
            get { return _searchText; }
            set
            {
                this.SetProperty(ref _searchText, value);
                NotifyOfPropertyChange("Instructions");
            }
        }
        
        private readonly InstructionProvider _instructionProvider;
        private IEnumerable<Instruction> _instructions;
        private readonly IEventAggregator _eventAggregator;
        private Instruction _selectedInstruction;
        private string _searchText;

        private IContextBroker _contextBroker;
        private readonly Subject<InstructionContext> _instructionContext = new Subject<InstructionContext>();

        public IEnumerable<Instruction> Instructions
        {
            get {
                return !string.IsNullOrWhiteSpace(SearchText) 
                    ? _instructions.Where(t => t.Name.Contains(SearchText)) 
                    : _instructions;
            }
            set { this.SetProperty(ref _instructions, value); }
        }

        public Instruction SelectedInstruction
        {
            get { return _selectedInstruction; }
            set
            {
                this.SetProperty(ref _selectedInstruction, value);
                SendMessage(_selectedInstruction);
            }
        }

        private async void SendMessage(Instruction instruction)
        {
            var message = new InstructionContext { Instruction = instruction };
            await _eventAggregator.PublishOnUIThreadAsync(message);
        }

        public InstructionManagerViewModel(IWindowManager windowManager, 
            IEventAggregator eventAggregator, 
            InstructionProvider instructionProvider, 
            string searchText,
            IContextBroker contextBroker)
            : base(windowManager)
        {
            _eventAggregator = eventAggregator;
            _instructionProvider = instructionProvider;
            SearchText = searchText;

            _eventAggregator.Subscribe(this);
            _contextBroker = contextBroker;

            _contextBroker.RegisterProvider(this, _instructionContext);
            _contextBroker.RegisterListener(Observer.Create<InstructionContext>(OnAction));
        }

        private void OnAction(InstructionContext obj)
        {
            
        }


        public void OnLoadData()
        {
            Instructions = _instructionProvider.Get();
        }

        public void OnShowDetail()
        {
            var instructionContext = new InstructionContext
            {
                Instruction = new Instruction
                {
                    Id = 1,
                    Name = "abc"
                }
            };

            _instructionContext.OnNext(instructionContext);
        }
    }
}
