﻿using MyWpf.Common.MessageBroker;
using MyWpf.Entities;

namespace MyWpf.Components.Instructions.Context
{
    public class InstructionContext : ContextBase
    {
        public Instruction Instruction { get; set; }
    }
}
