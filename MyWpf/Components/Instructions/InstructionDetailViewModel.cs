﻿using System.Collections.Generic;
using Caliburn.Micro;
using MyWpf.AppCommon;
using MyWpf.Components.Instructions.Context;
using MyWpf.Entities;
using MyWpf.Providers;
using MyWpf.SysCommon;

namespace MyWpf.Components.Instructions
{
    public class InstructionDetailViewModel : BaseEditorViewModel, IHandle<InstructionContext>
    {
        private readonly IEventAggregator _eventAggregator;
        private readonly ProductProvider _productProvider;
        private IEnumerable<Product> _products;

        public IEnumerable<Product> Products
        {
            get { return _products; }
            set { this.SetProperty(ref _products, value); }
        }

        public InstructionDetailViewModel(IWindowManager windowManager, IEventAggregator eventAggregator, ProductProvider productProvider)
            : base(windowManager)
        {
            _eventAggregator = eventAggregator;
            _productProvider = productProvider;

            _eventAggregator.Subscribe(this);
        }
        
        public void Handle(InstructionContext message)
        {
            Products = message.Instruction.Products;
        }
    }
}
