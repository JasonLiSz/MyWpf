﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;

namespace MyWpf.Components.MarketPrices
{
    public class PriceColorConverter: IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            SolidColorBrush brush = null;

            dynamic row = values[0];
            
            brush = new SolidColorBrush(Colors.Green);

            return brush;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
