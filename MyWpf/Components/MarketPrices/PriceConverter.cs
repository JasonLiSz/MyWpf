﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace MyWpf.Components.MarketPrices
{
    public class PriceConverter: IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            dynamic row = value;
            if (row == null) return 0;

            return row.Price>50 ? 100 : 80;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
