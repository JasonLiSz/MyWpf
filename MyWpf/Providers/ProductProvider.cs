﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Caliburn.Micro;
using MyWpf.Entities;

namespace MyWpf.Providers
{
    public class ProductProvider
    {
        private static readonly IObservableCollection<Product> Instructions = new BindableCollection<Product>();

        public ProductProvider()
        {
            Instructions.AddRange(
                new List<Product>
                {
                    new Product{ProductId = "001", Name = ""},
                    new Product{ProductId = "002", Name = ""},
                    new Product{ProductId = "003", Name = ""},
                    new Product{ProductId = "004", Name = ""},
                    new Product{ProductId = "005", Name = ""},

                    new Product{ProductId = "101", Name = ""},
                    new Product{ProductId = "102", Name = ""},
                    new Product{ProductId = "103", Name = ""},
                    new Product{ProductId = "104", Name = ""},
                    new Product{ProductId = "105", Name = ""},

                    new Product{ProductId = "201", Name = ""},
                    new Product{ProductId = "202", Name = ""},
                    new Product{ProductId = "203", Name = ""},
                    new Product{ProductId = "204", Name = ""},
                    new Product{ProductId = "205", Name = ""},
                });
        }

        public IEnumerable<Product> Get(Expression<Func<Product, bool>> filter = null)
        {
            return filter == null
                ? Instructions
                : Instructions.Where(filter.Compile());
        }
    }
}
