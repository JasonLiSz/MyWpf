﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Caliburn.Micro;
using MyWpf.Entities;

namespace MyWpf.Providers
{
    public class InstructionProvider
    {
        private readonly ProductProvider _productProvider;
        private static readonly IObservableCollection<Instruction> Instructions = new BindableCollection<Instruction>();

        public InstructionProvider(ProductProvider productProvider)
        {
            _productProvider = productProvider;

            Instructions.AddRange(
                new List<Instruction>
                {
                    new Instruction{Id = 1, Name = "中投1号", Side = Side.Buy, TotalAmount = 100000, Products = _productProvider.Get(t=>t.IsBond())},
                    new Instruction{Id = 2, Name = "蓝筹先锋", Side = Side.Buy, TotalAmount = 200000, Products = _productProvider.Get(t=>t.IsStock())},
                    new Instruction{Id = 3, Name = "新能源II期", Side = Side.Buy, TotalAmount = 300000, Products = _productProvider.Get(t=>t.IsSwap())}
                });
        }

        public IEnumerable<Instruction> Get(Expression<Func<Instruction, bool>> filter = null)
        {
            return filter == null
                ? Instructions
                : Instructions.Where(filter.Compile());
        }
    }
}
