﻿using System;
using System.Collections.Generic;
using System.Reactive.Concurrency;
using System.Reactive.Subjects;
using System.Threading;
using System.Timers;
using Caliburn.Micro;
using MyWpf.Entities;
using MyWpf.AppCommon;

namespace MyWpf.Providers
{
    public interface IMarketPriceProvider
    {
        IObservableCollection<MarketPrice> MarketPrices { get; }
    }

    public class MarketPriceProvider : IMarketPriceProvider
    {
        private readonly System.Timers.Timer _timer;
        private readonly Dictionary<string, ISubject<MarketPrice>> _localPriceCache = new Dictionary<string, ISubject<MarketPrice>>();

        public MarketPriceProvider()
        {
            MarketPrices = new BindableCollection<MarketPrice>
            {
                new MarketPrice{ ProductId = "160010.IB", Price = 100},
                new MarketPrice{ ProductId = "160011.IB", Price = 100},
                new MarketPrice{ ProductId = "160012.IB", Price = 100},
                new MarketPrice{ ProductId = "160013.IB", Price = 100},
                new MarketPrice{ ProductId = "160014.IB", Price = 100},
            };

            _timer = new System.Timers.Timer { Interval = 500 };
            _timer.Elapsed += SimulateMarketPrice;
            _timer.Enabled = true;
        }

        private void SimulateMarketPrice(object sender, ElapsedEventArgs e)
        {
            MarketPrices.ForEach(t =>
            {
                t.Price = new Random().Next(1, 100);
            });
        }

        public IObservableCollection<MarketPrice> MarketPrices { get; private set; }
    }
}
