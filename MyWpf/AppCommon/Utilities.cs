﻿using System;
using System.Collections.Generic;

namespace MyWpf.AppCommon
{
    public static class Utilities
    {
        public static void ForEach<T>(this IEnumerable<T> items, Action<T> action)
        {
            foreach (var item in items)
            {
                action(item);
            }
        }
    }
}
