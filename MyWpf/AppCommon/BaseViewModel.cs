﻿using Caliburn.Micro;
using MyWpf.SysCommon;

namespace MyWpf.AppCommon
{
    public abstract class BaseViewModel : PropertyChangedBase
    {
        protected readonly IWindowManager WindowManager;

        protected BaseViewModel(IWindowManager windowManager)
        {
            WindowManager = windowManager;
        }

        protected void ShowWindow<T>(bool isShowDailog = false)
        {
            if (!isShowDailog)
                WindowManager.ShowWindow(Resolver.Resolve<T>());
            else
                WindowManager.ShowDialog(Resolver.Resolve<T>());
        }
    }

    public abstract class BaseManagerViewModel : BaseViewModel
    {
        protected BaseManagerViewModel(IWindowManager windowManager) : base(windowManager)
        {

        }
    }

    public abstract class BaseEditorViewModel : BaseViewModel
    {
        protected BaseEditorViewModel(IWindowManager windowManager) : base(windowManager)
        {

        }
    }
}
