﻿using System.Windows;
using Caliburn.Micro;
using MyWpf.AppCommon;
using MyWpf.SysCommon;

namespace MyWpf.Researches
{
    public class CaliburnCommandViewModel : BaseManagerViewModel
    {
        public CaliburnCommandViewModel(IWindowManager windowManager)
            : base(windowManager)
        {

        }

        public void NoParameterCommand()
        {
            MessageBox.Show("No parameter command is fired.");
        }

        public void ParameterCommand(string name)
        {
            MessageBox.Show(name);

            WindowManager.ShowWindow(Resolver.Resolve<CaliburnCommandSubViewModel>());
        }
    }
}
