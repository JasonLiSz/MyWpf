﻿namespace MyWpf.Entities
{
    public class Product
    {
        public string ProductId { get; set; }
        public string Name { get; set; }
    }

    public static class ProductExt
    {
        public static bool IsBond(this Product product)
        {
            return product.ProductId.StartsWith("0");
        }

        public static bool IsStock(this Product product)
        {
            return product.ProductId.StartsWith("1");
        }

        public static bool IsSwap(this Product product)
        {
            return product.ProductId.StartsWith("2");
        }
    }
}