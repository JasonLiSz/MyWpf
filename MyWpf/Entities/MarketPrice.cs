﻿using Caliburn.Micro;
using MyWpf.SysCommon;

namespace MyWpf.Entities
{
    public class MarketPrice : PropertyChangedBase
    {
        private decimal _price;
        private string _productId;
        
        public string ProductId
        {
            get => _productId;
            set => this.SetProperty(ref _productId, value);
        }

        public decimal Price
        {
            get => _price;
            set => this.SetProperty(ref _price, value);
        }
    }
}