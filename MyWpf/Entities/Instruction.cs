﻿using System.Collections.Generic;

namespace MyWpf.Entities
{
    public enum Side
    {
        Buy, Sell
    }

    public class Instruction
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public Side Side { get; set; }
        public decimal TotalAmount { get; set; }

        public IEnumerable<Product> Products { get; set; }
    }
}
