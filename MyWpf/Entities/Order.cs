﻿namespace MyWpf.Entities
{
    public class Order
    {
        public int Id { get; set; }
        public int InstructionId { get; set; }
        public string ProductId { get; set; }
        public Side Side { get; set; }
        public decimal Quantity { get; set; }
        public decimal Price { get; set; }
        public decimal TotalAmount { get { return Quantity * Price; } }
    }
}
