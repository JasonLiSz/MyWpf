﻿using Caliburn.Micro;
using MyWpf.Common.MessageBroker;
using MyWpf.Components.Instructions;
using MyWpf.Components.MarketPrices;
using MyWpf.Providers;
using MyWpf.Researches;

namespace MyWpf.SysCommon
{
    public static class ComponentRegister
    {
        public static void Regist(SimpleContainer container)
        {
            RegistSystemComponents(container);

            RegistProductModels(container);
            RegistInstructionModels(container);
            RegistOrdernModels(container);
            RegistMarketPriceModels(container);

            RegistResearchModels(container);
        }

        private static void RegistMarketPriceModels(SimpleContainer container)
        {
            container.PerRequest<MarketPriceViewModel>();

            container.Singleton<IMarketPriceProvider, MarketPriceProvider>();
        }

        private static void RegistSystemComponents(SimpleContainer container)
        {
            container.Singleton<IWindowManager, WindowManager>();
            container.Singleton<IEventAggregator, EventAggregator>();

            container.Singleton<MainViewModel>();

            container.Singleton<IContextBroker, ContextBroker>();
        }

        private static void RegistProductModels(SimpleContainer container)
        {
            container.Singleton<ProductProvider>();
        }

        private static void RegistInstructionModels(SimpleContainer container)
        {
            container.Singleton<InstructionProvider>();
            
            container.PerRequest<InstructionManagerViewModel>();
            container.PerRequest<InstructionDetailViewModel>();
        }

        private static void RegistOrdernModels(SimpleContainer container)
        {
            
        }

        private static void RegistResearchModels(SimpleContainer container)
        {
            container.PerRequest<CaliburnCommandViewModel>();
            container.PerRequest<CaliburnCommandSubViewModel>();
        }
    }
}
