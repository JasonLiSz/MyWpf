﻿using Caliburn.Micro;

namespace MyWpf.SysCommon
{
    public static class Resolver
    {
        public static T Resolve<T>()
        {
            return IoC.Get<T>();
        }
    }
}
