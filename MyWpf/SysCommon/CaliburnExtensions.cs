﻿using System.Collections.Generic;
using System.Runtime.CompilerServices;
using Caliburn.Micro;

namespace MyWpf.SysCommon
{
    public static class CaliburnExtensions
    {
        public static bool SetProperty<T>(this PropertyChangedBase notificationObject, ref T property, T value, [CallerMemberName] string propertyName = null)
        {
            var result = !EqualityComparer<T>.Default.Equals(property, value);

            if (result)
            {
                property = value;
                notificationObject.NotifyOfPropertyChange(propertyName);
            }

            return result;
        }
    }
}
