﻿namespace MyWpf.Common.MessageBroker
{
    public interface IContext
    {

    }

    public class ContextBase : IContext
    {
        public string ContextGroup { get; set; }
        public string Identifier { get; set; }
    }
}
