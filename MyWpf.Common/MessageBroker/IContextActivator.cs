﻿using System.Reactive;

namespace MyWpf.Common.MessageBroker
{
    public enum ActivationOption { ExistingOrNew, NewInstance }

    public interface IContextActivator
    {

    }

    public interface IActivationContext : IContext
    {
        ActivationOption ActivationOption { get; }
    }

    public class ActivationContext : IActivationContext
    {
        public ActivationOption ActivationOption { get; set; }
    }

    public class ViewActivationContext<T> where T : IActivationContext
    {
        public ViewActivationContext(IContextBroker broker)
        {
            broker.RegisterListener(Observer.Create<T>(OnContext));
        }

        private void OnContext(T context)
        {
            
        }
    }
}
