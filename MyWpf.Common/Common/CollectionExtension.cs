﻿using System.Collections.Generic;

namespace MyWpf.Common.Common
{
    public static class CollectionExtension
    {
        public static TValue ValueOf<TKey, TValue>(this IDictionary<TKey, TValue> items, TKey key) where TValue : class
        {
            return items.TryGetValue(key, out var value) ? value : null;
        }
    }
}
